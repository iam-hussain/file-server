const sharp = require('sharp');

const fileName = [
  'Peacock_sm.png',
];

const inputFolder = '/Volumes/Code/my-projects/file-server/public/peacock/brand copy/';
const outputFolder = '/Volumes/Code/my-projects/file-server/public/peacock/brand/';

async function resizeImage() {
  try {
    const mapper = fileName.map(async (each) => {
      const input = `${inputFolder}${each}`;
      const output = `${outputFolder}${each}`;
      await sharp(input)
        .extract({ width: 700, height: 500, left: 250, top: 65 })
        .resize({
          width: 180,
          height: 110,
          fit: sharp.fit.cover,
        })
        .png({ compressionLevel: 9 }) // 0 to 9
        .jpeg({
          quality: 85, // 0 to 100
          chromaSubsampling: '4:4:4',
        })
        .toFile(output);
    });
    await Promise.allSettled(mapper);
  } catch (error) {
    console.log(error);
  }
}

resizeImage();
