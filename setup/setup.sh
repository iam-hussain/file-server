# Create a file deploy.sh and place this code
# Then run `bash deploy.sh`

echo "Preparing the server"

sudo apt update && sudo apt -y upgrade

echo "Installing Git"

cd ~

sudo apt install git

git --version

echo "Installing Node.JS and NPM"

cd ~

curl -sL https://deb.nodesource.com/setup_16.x -o nodesource_setup.sh

sudo bash nodesource_setup.sh

sudo apt-get install -y nodejs

sudo apt install npm

node -v && npm -v

echo "Installing Yarn"

sudo npm install --global yarn

yarn --version

echo "Installing Nignx"

sudo apt update

sudo apt install -y nginx

# sudo ufw app list

# sudo ufw allow 'Nginx HTTP'
# sudo ufw allow 'Nginx HTTPS'

# sudo ufw status

# # Run if inactive
# sudo ufw enable

systemctl status nginx

echo "Install PM2/NX"
sudo npm install -g nx
sudo npm install -g pm2

echo "Genrate SSH"
ssh-keygen -t ed25519 -C "itsjakirhussain@gmail.com"
eval $(ssh-agent -s)
ssh-add /home/ubuntu/.ssh/id_XXXXXXX.pub

echo "Install  SSH"
curl -L "https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh" | sudo bash
sudo apt-get update
sudo apt-get install gitlab-runner

wget https://fastdl.mongodb.org/tools/db/mongodb-database-tools-ubuntu2004-x86_64-100.5.4.deb
sudo apt -y install ./mongodb-database-tools-*-100.5.4.deb

# --------------------------**********************-----------------------------

# #### add ssh to the git
cat /home/ubuntu/.ssh/id_XXXXXXX

cd ~
git clone git@gitlab.com:iam-hussain/projects.git
cd ~/projects
npm ci
npm run build

# --------------------------**********************-----------------------------

# Do nginx configuratuion

sudo rm -f /etc/nginx/sites-available/default

sudo nano /etc/nginx/sites-available/default

# paste the nginx config

sudo nginx -t
sudo systemctl reload nginx
sudo systemctl restart nginx
sudo systemctl status nginx

sudo apt update
sudo apt install certbot
sudo apt install python3-certbot-nginx
sudo certbot --nginx -d jhussain.com -d www.jhussain.com -d file.jhussain.com -d peacock.jhussain.com -d club.jhussain.com
sudo certbot --nginx -d iam-hussain.site -d www.iam-hussain.site -d file.iam-hussain.site -d peacock.iam-hussain.site -d club.iam-hussain.site
# sudo certbot --nginx --expand  -d jhussain.com,cappido-api.jhussain.com,cappido.jhussain.com,stg-cappido-api.jhussain.com,stg-cappido.jhussain.com
sudo systemctl reload nginx
sudo systemctl restart nginx
sudo systemctl status nginx

# --------------------------**********************-----------------------------

echo "PM2 start"
cd ~/projects
pm2 start npm --name "file-manager" -- run "start:file-manager"
pm2 start npm --name "peacock-server" -- run "start:peacock-server"
pm2 start npm --name "peacock-client" -- run "start:peacock-client"
pm2 start npm --name "portfolio" -- run "start:portfolio"

cd ~/projects/file-server/
pm2 start npm --name "file-server" -- run "start"
cd ~/projects/peacock-verso/
pm2 start npm --name "peacock-verso" -- run "start"
cd ~/projects/peacock-recto/
pm2 start npm --name "peacock-recto" -- run "start"
cd ~/projects/my-portfolio/
pm2 start npm --name "my-portfolio" -- run "start"

# cd ~/stg/ledger/
# pm2 start npm --name "stg-ledger" -- run "start"

cd ~/projects/file-server/
npm run build
cd ~/projects/peacock-verso/
npm run build
cd ~/projects/peacock-recto/
npm run build
cd ~/projects/my-portfolio/
npm run build

cd ~/projects/file-server/
git pull
cd ~/projects/peacock-verso/
git pull
cd ~/projects/peacock-recto/
git pull
cd ~/projects/my-portfolio/
git pull

cd ~/projects/file-server/
npm ci
cd ~/projects/peacock-verso/
npm ci
cd ~/projects/peacock-recto/
npm ci
cd ~/projects/my-portfolio/
npm ci
cd ..

# run in your machine if needed
ssh-keygen -R ec2-52-66-137-232.ap-south-1.compute.amazonaws.com

sudo apt install redis-server
sudo nano /etc/redis/redis.conf
sudo systemctl restart redis.service
sudo systemctl status redis

Run this - -a www-data your_proj_username >gpasswd

Reload nginx - -s reload >nginx

Check chmod for your dirs: /home, /home/proj_dir, /home/proj_dir/static

Run this - stat --format '%a' /home . Result must be 755
Run this - stat --format '%a' /home/your_proj_dir/static . Result must be 755
Run this - stat --format '%a' /home/your_proj_dir . Result must be 750
If you have different values you can try to change this:
sudo chmod 755 /home
sudo chmod 755 /home/your_proj_dir/static
sudo chmod 750 /home/your_proj_dir
Reload you project-server. This solve all permission errors
