import dotenv from 'dotenv';
import Log from '../utils/logger';

Log.info('ENV :: Registering the env file');
dotenv.config();

Log.info('ENV :: Initializes the configs');
export const ENV = process.env.NODE_ENV || 'development';
export const PORT = process.env.PORT || 5000;
export const ENABLE_CHILD_PROCESS = process.env.ENABLE_CHILD_PROCESS === 'YES';
export const IS_PRODUCTION = ENV === 'production';
export const APP_SECRET = process.env.APP_SECRET || '98uhu5rfs0cve';

export default {
  PORT,
  ENV,
  ENABLE_CHILD_PROCESS,
  IS_PRODUCTION,
  APP_SECRET,
};
