import express from 'express';
import cors from 'cors';
import serveIndex from 'serve-index';
import Log, { httpLogger } from '../utils/logger';
import { PORT } from './Configs';
import { errorHandler, notFoundHandler } from '../utils/exception';

export default () => {
  Log.info('Express :: Initializes the express server');
  let app = express();

  Log.info('Middleware :: Booting the middleware...');
  // middleware that can be used to enable CORS
  app.use(cors());
  // Enables the request body parser
  app.use(express.json()); // for parsing application/json
  app.use(express.urlencoded({ extended: true })); // for parsing application/x-www-form-urlencoded

  app.use(httpLogger);

  Log.info('Static :: Initializes the serve index');
  app.use(express.static('public'));
  app.use('/ftp', express.static('public'), serveIndex('public', { icons: true }));

  Log.info('Exception :: Registering Exception/Error Handlers...');
  app.use(errorHandler);
  app = notFoundHandler(app);

  // listens for connections on the specified ports
  app.listen(PORT, (_error) => {
    if (_error) {
      return Log.error(_error);
    }
    return Log.info(`Listening :: Server is running @ ${PORT}`);
  });
  return app;
};
